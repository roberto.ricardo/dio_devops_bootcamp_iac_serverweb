#!/bin/bash

#Atualiza o servidor
apt update -y
apt upgrade -y

#Instala o Apache 2
apt install apache2 -y

#Instala o Unzip
apt install unzip -y

#Baixa o aplicativo na pasta tmp e decompacta o arquivo
cd /tmp/
wget https://github.com/denilsonbonatti/linux-site-dio/archive/refs/heads/main.zip
unzip main.zip
cd linux-site-dio-main/

#Copia os arquivos da pasta temp para a pasta do server
cp -R * /var/www/html/
