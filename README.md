# DIO_DevOps_BootCamp_IAC_ServerWeb

# Definições

Restaurar o snapshot criado anteriormente no virtualbox;&nbsp;
Atualizar o servidor;&nbsp;
Instalar o apache2;
Instalar o unzip;
Baixar a aplicação disponível no endereço https://github.com/denilsonbonatti/linux-site-dio/archive/refs/heads/main.zip no diretório /tmp;
Copiar os arquivos da aplicação no diretório padrão do apache;
Subir arquivo de script para um repositório no GitHub.
